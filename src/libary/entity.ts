import Vector2 from './vector2';
import { Component } from '@angular/core';
import { DomService } from '../services/dom.service';

export enum IEntityType {
  TOWER,
  EMPTY
}

export class Entity {
  type: IEntityType;
  color: string;
  position: Vector2;
  debug = false;

  constructor(position: Vector2 = new Vector2(0,0,), type: IEntityType = IEntityType.EMPTY, color: string = '#397A19') {
    this.type     = type;
    this.position = position;
    this.color    = color;
  }

  setPosition(vec: Vector2) {
    this.position = vec;
  }
}

export class Tower extends Entity {
  public damage: number;
  public projectile: string;

  constructor(position: Vector2 = new Vector2(0,0,), damage:number,  color: string = '#397A19') {
    const type: IEntityType = IEntityType.TOWER;
    super(position, type, color);

    this.damage = damage;
  }

  shootAt(pos: Vector2, dom: DomService, comp: Projectile) {
    comp.target = pos;
    dom.appendComponentToBody(comp);
  }
}

export class BaseTower extends Tower {
  constructor(position: Vector2 = new Vector2(0,0)) {
    const color = '#7A5000';
    const damage = 1;

    super(position, damage, color);
  }
}

type Projectile = (ProjectileBullet);

@Component({
  selector: 'projectile-bullet',
  template: '<div>O</div>',
})
export class ProjectileBullet {
  public target : Vector2;

  constructor ( ) {
    console.log("IT WORKED!!");
  }
}
