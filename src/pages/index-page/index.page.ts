import { Component } from '@angular/core';

@Component({
  selector: 'indexPage',
  template: require('./index.page.html'),
  styles: [ require('./index.page.scss') ]
})
export class IndexPage {
  constructor() {
  }
}
