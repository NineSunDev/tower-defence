import { NgModule } from '@angular/core';

import {
  RouterModule,
  Routes
} from '@angular/router';
import { IndexPage } from './pages/index-page/index.page';

const appRoutes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/index'
  },
  {
    path: 'index',
    component: IndexPage
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {useHash: true})
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
