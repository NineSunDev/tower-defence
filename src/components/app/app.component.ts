import { Component } from '@angular/core';

@Component({
  selector: 'taw-base',
  template: require('./app.component.html'),
  styles: [ require('./app.component.scss') ]
})
export class AppComponent {

  constructor() {
  }
}
