import { ChangeDetectorRef, Component } from '@angular/core';
import { BaseTower, Entity, IEntityType } from '../../libary/entity';
import Vector2 from '../../libary/vector2';

let PF = require('pathfinding');


@Component({
  selector: 'game-tower-defence',
  template: require('./game-tower-defence.component.html'),
  styles: [require('./game-tower-defence.component.scss')],
})
export class GameTowerDefenceComponent {

  public readonly gridSize: number = 35;
  public grid: Entity[][]          = [[]];
  public pfGrid: any               = null;
  public finder: any               = new PF.AStarFinder();
  public activePath: any = [];

  public start: Vector2 = new Vector2(0, 0);
  public end: Vector2   = new Vector2(25, 20);

  public draggedTower: any = null;

  public possibleTower: (Entity | BaseTower)[] = [
    new BaseTower(),
  ];

  constructor(private cdf: ChangeDetectorRef) {
    for (let y = 0; y < this.end.y; y++) {
      this.grid.push([]);
      for (let x = 0; x < this.end.x; x++) {
        this.grid[y][x] = new Entity(new Vector2(x, y), IEntityType.EMPTY);
      }
    }

    this.pfGrid = new PF.Grid(this.end.x, this.end.y);
  }

  onDrag(event: DragEvent, towerID: number) {
    this.draggedTower = this.possibleTower[towerID];
  }

  onDragEnter(event: DragEvent) {
    const target: HTMLElement = event.target as HTMLElement;
    const position: Vector2   = new Vector2(Number(target.dataset.positionx), Number(target.dataset.positiony));

    if (this.draggedTower && this.grid[position.y][position.x].type == IEntityType.EMPTY) {
      this.grid[position.y][position.x].color = this.draggedTower.color;
      this.cdf.detectChanges();
    }
  }

  onDrop(event: DragEvent) {
    const target: HTMLElement = event.target as HTMLElement;
    const position: Vector2   = new Vector2(Number(target.dataset.positionx), Number(target.dataset.positiony));

    if (this.draggedTower && this.grid[position.y][position.x].type == IEntityType.EMPTY) {
      let newPath:any;
      if ((newPath = this.getPath(position)).length !== 0) {
        this.activePath = newPath;

        this.grid[position.y][position.x]          = Object.assign({}, this.draggedTower);
        this.grid[position.y][position.x].position = Object.assign({}, position);

        this.pfGrid.setWalkableAt(position.x, position.y, false);

        // this.grid[position.y][position.x].shootAt(new Vector2(0,0));

      } else {

        this.grid[position.y][position.x] = Object.assign({}, new Entity(position));

      }
    }

    this.debugPath();
  }

  onDragOver(event: DragEvent) {
    event.preventDefault();
    event.stopPropagation();
  }

  onDragLeave(event: DragEvent) {
    const target: HTMLElement = event.target as HTMLElement;
    const position: Vector2   = new Vector2(Number(target.dataset.positionx), Number(target.dataset.positiony));

    if (this.grid[position.y][position.x].type == IEntityType.EMPTY) {
      this.grid[position.y][position.x].color = '#397A19';
    }
  }

  getPath(pos: Vector2): any[] {
    const walkGrid: any = this.pfGrid.clone();
    walkGrid.setWalkableAt(pos.x, pos.y, false);

    return this.finder.findPath(this.start.x, this.start.y, this.end.x-1, this.end.y-1, walkGrid);
  }

  debugPath(){
    for (let y = 0; y < this.end.y; y++) {
      for (let x = 0; x < this.end.x; x++) {
        this.grid[y][x].debug = false;
      }
    }

    for (let step of this.activePath) {
      this.grid[step[1]][step[0]].debug = true;
    }
  }

}
