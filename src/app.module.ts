import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components/app/app.component';
import { IndexPage } from './pages/index-page/index.page';
import { GameTowerDefenceComponent } from './components/game-tower-defence/game-tower-defence.component';
import { DomService } from './services/dom.service';
import { ProjectileBullet } from './libary/entity';




@NgModule({
  bootstrap: [AppComponent],
  declarations: [
    AppComponent,
    GameTowerDefenceComponent,

    IndexPage
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
  ],
  providers: [
    DomService
  ],
  entryComponents: [
    ProjectileBullet
  ]
})
export class AppModule {
}
